<?php

namespace App\EventSubscriber;

use App\Entity\HttpCall;
use App\Entity\HttpError;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ExceptionSubscriber implements EventSubscriberInterface
{
    private $em;

    public function __construct(EntityManagerInterface $em){
        $this->em = $em;
    }

    public function onKernelException(ExceptionEvent $event)
    {
        if($event instanceof HttpError){
        $httpError = new HttpError();
        $httpError->setCodeHttp($event->getThrowable()->getStatusCode());
        $httpError->setMessage($event->getThrowable()->getMessage());

        $this->em->persist($httpError);
        $this->em->flush();
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.exception' => 'onKernelException',
        ];
    }
}
