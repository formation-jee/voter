<?php

namespace App\EventSubscriber;

use App\Entity\HttpCall;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class RequestSubscriber implements EventSubscriberInterface
{
    private $em;

    public function __construct(EntityManagerInterface $em){
        $this->em = $em;
    }
    public function onKernelRequest(RequestEvent $event)
    {


        $httpCall = new HttpCall();
        $httpCall->setDateAppel(new \DateTime());
        $httpCall->setUrl($event->getRequest()->getPathInfo());

        $this->em->persist($httpCall);
        $this->em->flush();


    }

    public static function getSubscribedEvents()
    {
        return [
            RequestEvent::class => 'onKernelRequest',
        ];
    }
}