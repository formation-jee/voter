<?php
namespace App\EventSubscriber;

use App\Entity\Sejour;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class SejourSubscriber implements EventSubscriber {

    public function getSubscribedEvents()
    {
       return [
           Events::prePersist,
           Events::preUpdate,
       ];
    }

    public function prePersist(LifecycleEventArgs  $args){

        if($args->getObject() instanceof Sejour){
            $sejour = $args->getObject();
            $sejour->setDateAjout(new \DateTime());
        }
    }

    public function preUpdate(LifecycleEventArgs  $args){
        if($args->getObject() instanceof Sejour){
            $sejour = $args->getObject();
            $sejour->setDateModif(new \DateTime());
        }
    }
}
