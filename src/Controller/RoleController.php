<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\SejourType;
use App\Form\UserRightType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/{_locale}")
 *     requirements={
 *         "_locale": "en|fr",
 *         "_format": "html|xml",
 *     })
 */
class RoleController extends AbstractController
{
    private $manager;
    private $userRepository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->manager = $em;

        $this->userRepository = $em->getRepository(User::class);
    }

    /**
     * @Route("/admin/role", name="role")
     */
    public function index(): Response
    {
        $allUser = $this->userRepository->findAll();

        return $this->render('role/index.html.twig', [
            'controller_name' => 'RoleController',
            'users' => $allUser
        ]);
    }

    /**
     * @Route("/admin/role/{id}", name="role_form")
     */
    public function getForm(User $user, Request $request): Response
    {

        $form = $this->createForm(UserRightType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            $this->manager->persist($user);
            $this->manager->flush();

            return  $this->redirectToRoute('role');
        }


        return $this->render('role/form.html.twig', [
            'controller_name' => 'RoleController',
            'form' => $form->createView()

        ]);
    }

}
