<?php

namespace App\Controller;

use App\Entity\Sejour;
use App\Entity\SejourUser;
use App\Entity\User;
use App\Services\UidGeneratorService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/{_locale}",
 *     requirements={
 *         "_locale": "en|fr",
 *         "_format": "html|xml",
 *     })
 */
class DefaultController extends AbstractController
{
    private $sejourRepository;
    private $sejourUserRepo;

    private $manager;
    private $uidGenerator;

    public function __construct(EntityManagerInterface  $em, UidGeneratorService $uid){
        $this->manager = $em;
        $this->sejourRepository = $em->getRepository(Sejour::class);
        $this->sejourUserRepo = $em->getRepository(SejourUser::class);
        $this->uidGenerator = $uid;
    }

    /**
     * @Route("/", name="default")
     */
    public function index($_locale): Response
    {

        $sejours = $this->sejourRepository->findAll();

        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'sejours' => $sejours,
            'locale'=> $_locale
        ]);
    }

    /**
     * @Route("/like/{id}", name="default_like")
     */
    public function like(Sejour $sejour): Response
    {
        // Retourne l'utilisateur courant
        $user = $this->getUser();

        // recherche tous les votes sur le séjour passé dans l'url et l'utilisateur connecté

        $existingVote = $this->sejourUserRepo->findBy(['sejour'=> $sejour, 'userVote' => $this->getUser()]);

        // Si il a jamais voté je lui ajoute un vote sur l'événement
        // Sinon jirai édité son vote.
        if(count($existingVote) == 0){
            $sejourUser = new SejourUser();

            $sejourUser->setSejour($sejour);
            $sejourUser->setUserVote($user);
            $sejourUser->setVote(true);

            $this->manager->persist($sejourUser);
            $this->manager->flush();
        } else {
            $sejourUser = $existingVote[0];
            $sejourUser->setSejour($sejour);
            $sejourUser->setUserVote($user);
            $sejourUser->setVote(true);

            $this->manager->persist($sejourUser);
            $this->manager->flush();
        }


        return $this->redirectToRoute("default");
    }

    /**
     * @Route("/un-like/{id}", name="default_unlike")
     */
    public function unlike(Sejour $sejour): Response
    {

        // Meme fonction sauf que le vote est a false.
        $user = $this->getUser();

        // TODO ici : on vérifie qu'on a pas déjà un vote ! Sinon on l'update


        $existingVote = $this->sejourUserRepo->findBy(['sejour'=> $sejour, 'userVote' => $this->getUser()]);


        if(count($existingVote) === 0){
            $sejourUser = new SejourUser();

            $sejourUser->setSejour($sejour);
            $sejourUser->setUserVote($user);
            $sejourUser->setVote(false);

            $this->manager->persist($sejourUser);
            $this->manager->flush();
        } else {
            $sejourUser = $existingVote[0];
            $sejourUser->setSejour($sejour);
            $sejourUser->setUserVote($user);
            $sejourUser->setVote(false);

            $this->manager->persist($sejourUser);
            $this->manager->flush();
        }




        return $this->redirectToRoute("default");
    }
}
