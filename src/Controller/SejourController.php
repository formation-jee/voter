<?php

namespace App\Controller;

use App\Entity\Sejour;
use App\Form\SejourType;
use App\Repository\SejourRepository;
use App\Services\SejourService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("{_locale}/admin/sejour")
 *     requirements={
 *         "_locale": "en|fr",
 *         "_format": "html|xml",
 *     })
 */
class SejourController extends AbstractController
{
    private $sejourService;

    public function __construct(SejourService $sejourService)
    {
        $this->sejourService = $sejourService;
    }

    /**
     * @Route("/", name="sejour_index", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render('sejour/index.html.twig', [
            'sejours' => $this->sejourService->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="sejour_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {

        $sejour = new Sejour();
        $form = $this->createForm(SejourType::class, $sejour);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $sejour->setImage($this->sejourService->uploadImage($form, $this->getParameter('picture_sejour_folder')));

            $this->sejourService->add($sejour);


            return $this->redirectToRoute('sejour_index');
        }

        return $this->render('sejour/new.html.twig', [
            'sejour' => $sejour,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sejour_show", methods={"GET"})
     */
    public function show(Sejour $sejour): Response
    {
        return $this->render('sejour/show.html.twig', [
            'sejour' => $sejour,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="sejour_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Sejour $sejour): Response
    {
        $form = $this->createForm(SejourType::class, $sejour);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sejour_index');
        }

        return $this->render('sejour/edit.html.twig', [
            'sejour' => $sejour,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sejour_delete", methods={"POST"})
     */
    public function delete(Request $request, Sejour $sejour): Response
    {
        if ($this->isCsrfTokenValid('delete'.$sejour->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($sejour);
            $entityManager->flush();
        }

        return $this->redirectToRoute('sejour_index');
    }
}
