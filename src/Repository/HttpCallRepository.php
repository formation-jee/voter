<?php

namespace App\Repository;

use App\Entity\HttpCall;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method HttpCall|null find($id, $lockMode = null, $lockVersion = null)
 * @method HttpCall|null findOneBy(array $criteria, array $orderBy = null)
 * @method HttpCall[]    findAll()
 * @method HttpCall[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HttpCallRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HttpCall::class);
    }

    // /**
    //  * @return HttpCall[] Returns an array of HttpCall objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HttpCall
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
