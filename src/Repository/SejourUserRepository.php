<?php

namespace App\Repository;

use App\Entity\SejourUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SejourUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method SejourUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method SejourUser[]    findAll()
 * @method SejourUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SejourUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SejourUser::class);
    }

    // /**
    //  * @return SejourUser[] Returns an array of SejourUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SejourUser
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
