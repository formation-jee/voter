<?php

namespace App\Repository;

use App\Entity\Sejour;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Sejour|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sejour|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sejour[]    findAll()
 * @method Sejour[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SejourRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sejour::class);
    }

    public function findMaxLike(){
        return $this->createQueryBuilder('s')
            ->addSelect('COUNT(s) as nbElem')

            ->leftJoin('s.sejourUser', 'user')
            ->where('user.vote = :true')
            ->setParameter('true', true)
            ->groupBy('s.id')
            ->orderBy('nbElem', 'DESC')
            ->setMaxResults(1)
            ->getQuery()->getResult();
    }

    public function findUnLike(){
        return $this->createQueryBuilder('s')

            ->addSelect('COUNT(s) as nbElem')

            ->leftJoin('s.sejourUser', 'user')
            ->where('user.vote = :true')
            ->setParameter('true', false)
            ->groupBy('s.id')
            ->orderBy('nbElem', 'DESC')
            ->setMaxResults(1)
            ->getQuery()->getSQL();
    }


    // /**
    //  * @return Sejour[] Returns an array of Sejour objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Sejour
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
