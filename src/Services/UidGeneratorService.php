<?php
namespace App\Services;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;

class UidGeneratorService extends AbstractIdGenerator
{
    public function generate(EntityManager $em, $entity)
    {
        $uuid = uniqid();

        if (null !== $em->getRepository(get_class($entity))->findOneBy(['id' => $uuid])) {
            $uuid = $this->generate($em, $entity);
        }

        return $uuid;
    }
}
?>
