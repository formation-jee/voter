<?php


namespace App\Services;


use App\Entity\Sejour;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\String\Slugger\SluggerInterface;

class SejourService
{
    private $logger;
    private $manager;
    private $slugger;
    private $sejourRepo;

    public function __construct(LoggerInterface $logger, EntityManagerInterface  $em, SluggerInterface  $slugger){
        $this->logger = $logger;
        $this->manager = $em;
        $this->slugger = $slugger;
        $this->sejourRepo = $em->getRepository(Sejour::class);
    }

    public function add($sejour){
        $this->manager->persist($sejour);
        $this->manager->flush();




        $this->logger->info("Sejour ajouté !");
        return $sejour;
    }


    public function findAll(){
        return $this->sejourRepo->findAll();
    }

    public function getOne($id){
        return $this->sejourRepo->find($id);
    }

    public function saveOrUpdate($sejour){
        $this->manager->persist($sejour);
        $this->manager->flush();
    }

    public function  uploadImage($form, $uploadDir){
        $image = $form->get('image')->getData();
        if ($image) {
            $originalFilename = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $this->slugger->slug($originalFilename);
            $newFilename = $safeFilename.'-'.uniqid().'.'.$image->guessExtension();
            try {
                $image->move(
                    $uploadDir,
                    $newFilename
                );
            } catch (FileException $e) {
                die(dump('ERROR !!'));
            }

            return $newFilename;
        }
    }
}