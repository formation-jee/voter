<?php

namespace App\Entity;

use App\Repository\HttpErrorRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HttpErrorRepository::class)
 */
class HttpError
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $codeHttp;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $message;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeHttp(): ?int
    {
        return $this->codeHttp;
    }

    public function setCodeHttp(?int $codeHttp): self
    {
        $this->codeHttp = $codeHttp;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }
}
