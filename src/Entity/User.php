<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 */
class User implements UserInterface
{
    /**
     * @ORM\Column(name="id", type="string")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Services\UidGeneratorService")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity=SejourUser::class, mappedBy="userVote", orphanRemoval=true)
     */
    private $sejourUser;

    /**
     * @ORM\Column(type="boolean")
     */
    private $needRedefine;



    /**
     * @return mixed
     */
    public function isNeedRedefine()
    {
        return $this->needRedefine;
    }

    /**
     * @param mixed $needRedefine
     */
    public function setNeedRedefine($needRedefine): void
    {
        $this->needRedefine = $needRedefine;
    }

    public function __construct()
    {
        $this->sejourUser = new ArrayCollection();
        $this->setNeedRedefine(true);
    }

    public function hasRole($role){
        $return = false;

        foreach ($this->roles as $rol){
            if($rol === $role){
                $return = true;
            }
        }

        return $return;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function addRole($role){
        $this->roles[] = $role;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|SejourUser[]
     */
    public function getSejourUser(): Collection
    {
        return $this->sejourUser;
    }

    public function addSejourUser(SejourUser $sejourUser): self
    {
        if (!$this->sejourUser->contains($sejourUser)) {
            $this->sejourUser[] = $sejourUser;
            $sejourUser->setUserVote($this);
        }

        return $this;
    }

    public function removeSejourUser(SejourUser $sejourUser): self
    {
        if ($this->sejourUser->removeElement($sejourUser)) {
            // set the owning side to null (unless already changed)
            if ($sejourUser->getUserVote() === $this) {
                $sejourUser->setUserVote(null);
            }
        }

        return $this;
    }

}
