<?php

namespace App\Entity;

use App\Repository\SejourRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SejourRepository::class)
 */
class Sejour
{
    /**
     * @ORM\Column(name="id", type="string")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="App\Services\UidGeneratorService")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="datetime", length=255, nullable=true)
     */
    private $dateAjout;


    /**
     * @ORM\Column(type="datetime", length=255, nullable=true)
     */
    private $dateModif;



    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=SejourUser::class, mappedBy="sejour", orphanRemoval=true, fetch="EAGER")
     */
    private $sejourUser;

    public function __construct()
    {
        $this->sejourUser = new ArrayCollection();
    }

    public function getSejourUser()
    {
        return $this->sejourUser;
    }

    public function addSejour($sejour)
    {
        if (!$this->sejourUser->contains($sejour)) {
            $this->sejourUser->add($sejour);
        }
    }

    public function removeSejour($sejour)
    {
        if ($this->sejourUser->contains($sejour)) {
            $this->sejourUser->removeElement($sejour);
        }
    }


    public function getId(): ?string
    {
        return $this->id;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUserReaction($user)
    {
        $retour = "Je m'en tappe";
        foreach ($this->getSejourUser() as $sejour) {
            if ($sejour->getUserVote() == $user) {
                if ($sejour->getVote() == 1) {
                    $retour="J'aime !";
                } elseif ($sejour->getVote() == 0) {
                    $retour = 'J\'aime PAS !';
                }
            }
        }

        return $retour;
    }

    public function getNbLike()
    {
        $i = 0;
        foreach ($this->getSejourUser() as $sejour) {
            if ($sejour->getVote() == 1) {
                $i++;
            }
        }

        return $i;
    }

    public function getNbUnLike()
    {
        $i = 0;
        foreach ($this->getSejourUser() as $sejour) {
            if ($sejour->getVote() == 0) {
                $i++;
            }
        }
        return $i;
    }


    /**
     * @return mixed
     */
    public function getDateAjout()
    {
        return $this->dateAjout;
    }

    /**
     * @param mixed $dateAjout
     */
    public function setDateAjout($dateAjout): void
    {
        $this->dateAjout = $dateAjout;
    }

    /**
     * @return mixed
     */
    public function getDateModif()
    {
        return $this->dateModif;
    }

    /**
     * @param mixed $dateModif
     */
    public function setDateModif($dateModif): void
    {
        $this->dateModif = $dateModif;
    }



}
