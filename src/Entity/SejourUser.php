<?php

namespace App\Entity;

use App\Repository\SejourUserRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SejourUserRepository::class)
 */
class SejourUser
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $vote;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="sejourUser", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userVote;

    /**
     * @ORM\ManyToOne(targetEntity=Sejour::class, inversedBy="sejourUser")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sejour;

    /**
     * @return mixed
     */
    public function getSejour()
    {
        return $this->sejour;
    }

    /**
     * @param mixed $sejour
     */
    public function setSejour($sejour): void
    {
        $this->sejour = $sejour;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVote(): ?bool
    {
        return $this->vote;
    }

    public function setVote(bool $vote): self
    {
        $this->vote = $vote;

        return $this;
    }

    public function getUserVote(): ?User
    {
        return $this->userVote;
    }

    public function setUserVote(?User $userVote): self
    {
        $this->userVote = $userVote;

        return $this;
    }
}
