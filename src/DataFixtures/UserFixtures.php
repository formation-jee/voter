<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface  $passwordEncoder){
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $etudiants = [
            [
                "nom"=> "GOUPIL",
                "prenom"=> "Lilian",
                "isAdmin"=> true,
                "isSuperAdmin"=> false
            ],
            [
                "nom"=> "BOSSIN",
                "prenom"=> "Rodolphe",
                "isAdmin"=> true,
                "isSuperAdmin"=> false
            ],
            [
                "nom"=> "JACQUOT",
                "prenom"=> "Mickael",
                "isAdmin"=> true,
                "isSuperAdmin"=> false
            ],
            [
                "nom"=> "DELORME",
                "prenom"=> "Aurélien",
                "isAdmin"=> true,
                "isSuperAdmin"=> true
            ],
            [
                "nom"=> "tester",
                "prenom"=> "tester",
                "isAdmin"=> true,
                "isSuperAdmin"=> true
            ]
        ];


        foreach ($etudiants as $etudiant){
            $user = new User();

            if($etudiant['isAdmin']){
                $user->addRole("ROLE_ADMIN");
            }


            if($etudiant['isSuperAdmin']){
                $user->addRole("ROLE_SUPER_ADMIN");
            }

            $username = $etudiant["nom"]. '_' . $etudiant["prenom"];

            $user->setUsername($username);

            $user->setPassword($this->passwordEncoder->encodePassword($user, $username));

            $manager->persist($user);

        }
        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
