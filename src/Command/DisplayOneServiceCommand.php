<?php

namespace App\Command;

use App\Services\SejourService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DisplayOneServiceCommand extends Command
{
    protected static $defaultName = 'app:display-one-service';
    protected static $defaultDescription = 'Add a short description for your command';
    private $sejourService;

    public function __construct(string $name = null, SejourService $sejourService)
    {
        parent::__construct($name);
        $this->sejourService = $sejourService;
    }

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('arg1', InputArgument::REQUIRED, 'Argument description')

        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $io = new SymfonyStyle($input, $output);


        $serjour = $this->sejourService->getOne($input->getArgument("arg1"));

        $io->success("titre : ". $serjour->getTitre());
        $io->success("description : ". $serjour->getDescription());

        return Command::SUCCESS;
    }
}
