<?php

namespace App\Command;

use App\Entity\Sejour;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class TopSejourCommand extends Command
{
    protected static $defaultName = 'app:top-sejour';
    protected static $defaultDescription = 'Add a short description for your command';
    protected $sejourManager;

    public function __construct(string $name = null, EntityManagerInterface $entityManager)
    {
        parent::__construct($name);

        $this->sejourManager = $entityManager->getRepository(Sejour::class);

    }

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $maxSejourLike = $this->sejourManager->findMaxLike()[0][0]->getTitre();
        $io->success("Le séjour préféré est :". $maxSejourLike);


        return Command::SUCCESS;
    }
}
