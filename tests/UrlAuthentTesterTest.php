<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UrlAuthentTesterTest extends WebTestCase
{
    private $forbiddenUrls = [
        '/fr/',
        '/en/',
        '/en/like/1 ',
    ];

    private $authorizedUrl = [
        'http://localhost/fr/login'
    ];


    public function testSomething(): void
    {
        $client = static::createClient();

        foreach ($this->forbiddenUrls as $url) {
            $crawler = $client->request('GET', $url);

            $this->assertTrue($client->getResponse()->getStatusCode() == 302);
        }

        foreach ($this->authorizedUrl as $url) {
            $crawler = $client->request('GET', $url);

            $this->assertResponseIsSuccessful();
        }
    }
}
