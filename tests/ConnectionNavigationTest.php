<?php

namespace App\Tests;

use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ConnectionNavigationTest extends WebTestCase
{
    public function testText(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', 'http://localhost:8000/fr/login');

        $this->assertSelectorTextContains('h1', 'Please sign in');
    }

    public function testLoginStatus(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/fr/login');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testLoginForm(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/fr/login');

        $form = $crawler->selectButton('Sign in')->form();

        $form['username'] = 'tester_tester';
        $form['password'] = 'tester_tester';

        $crawler = $client->submit($form);

        $uri = $crawler->getUri();

        $this->assertEquals($client->followRedirect()->getUri(), "http://localhost/fr/reset-password");

        $exist = strpos( $client->getResponse()->getContent(),'<h1>Reset your password</h1>');

        $this->assertTrue($exist!=false);
    }
}
