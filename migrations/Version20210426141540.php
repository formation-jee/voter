<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210426141540 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE sejour (id VARCHAR(255) NOT NULL, image VARCHAR(255) DEFAULT NULL, titre VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sejour_user (id INT AUTO_INCREMENT NOT NULL, user_vote_id VARCHAR(255) NOT NULL, sejour_id VARCHAR(255) NOT NULL, vote TINYINT(1) NOT NULL, INDEX IDX_1521F10E2AD1C4F6 (user_vote_id), INDEX IDX_1521F10E84CF0CF (sejour_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (id VARCHAR(255) NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, need_redefine TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sejour_user ADD CONSTRAINT FK_1521F10E2AD1C4F6 FOREIGN KEY (user_vote_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE sejour_user ADD CONSTRAINT FK_1521F10E84CF0CF FOREIGN KEY (sejour_id) REFERENCES sejour (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sejour_user DROP FOREIGN KEY FK_1521F10E84CF0CF');
        $this->addSql('ALTER TABLE sejour_user DROP FOREIGN KEY FK_1521F10E2AD1C4F6');
        $this->addSql('DROP TABLE sejour');
        $this->addSql('DROP TABLE sejour_user');
        $this->addSql('DROP TABLE `user`');
    }
}
